import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async createPost(authorID: number, body: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body({
                authorID: authorID,
                body: body,
            })
            .send();
        return response;
    }

    async likePost(authorID: number, entityID: number) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                authorID: authorID,
                entityID: entityID,
            })
            .send();
        return response;
    }
}