import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class RegisterController {

    async registerUser(email, password) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Users`)
            .email(email)
            .password(password)
            .send();
        return response;
    }
  
}