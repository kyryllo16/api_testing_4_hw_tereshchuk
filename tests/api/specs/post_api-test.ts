import { expect } from "chai";
import { PostController } from "../lib/controllers/posts.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { xdescribe, before, it } from "node:test";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const post = new PostController();
const auth = new AuthController();
const schemas = require('./data/schemas_testData.json');
let authorID: number;
let entityID: number;
let body: string;

xdescribe("Token usage", () => {
    let accessToken: string;

    before( async () => {
        let response = await auth.login("kyryllo16@gmail.com", "p@ssword");
 
        accessToken = response.body.token.accessToken.token;
    });

    it(`should return 200 status code and all posts when getting the post collection`, async () => {
        let response = await post.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        expect(response.body).to.be.jsonSchema(schemas.schema_forPosts); 

        entityID = response.body[0].id;
        authorID = response.body[2].id;
    });

    it(`should return 200 status code and create a new post`, async () => {
        let response = await post.createPost(authorID, body);

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_forPosts); 
    });

    it(`should return 200 status code and all posts when getting the post collection`, async () => {
        let response = await post.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        expect(response.body).to.be.jsonSchema(schemas.schema_forPosts); 

        entityID = response.body[0].id;
        authorID = response.body[2].id;
    });

    it(`should return 200 status code and add like to post`, async () => {
        let response = await post.likePost(authorID, entityID);

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_forPosts); 
    });

    it(`should return 200 status code and all posts when getting the post collection`, async () => {
        let response = await post.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        expect(response.body).to.be.jsonSchema(schemas.schema_forPosts); 
    });

});
