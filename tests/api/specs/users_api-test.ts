import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime, checkId } from "../../helpers/functionsForChecking.helper";
import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { xdescribe, it } from "node:test";

const users = new UsersController();
const register = new RegisterController();
const auth = new AuthController();
const schemas = require('./data/schemas_testData.json');
const chai = require('chai');
let email: string;
let password: string;
let accessToken: string;
let userData: object;

chai.use(require('chai-json-schema'));

xdescribe(`Users controller`, () => {
    let userId: number;
 

    it(`should return 201 status code and register new user`, async () => {
        email = "kyryllo16@gmail.com";
        password = "p@ssword";
        let response = await register.registerUser(email, password);
        
        checkStatusCode(response, 201);
        checkResponseTime(response,1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_allUsers); 
        
        userId = response.body[1].id;
    });

    it(`should return 200 status code and all users when getting the user collection`, async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        expect(response.body).to.be.jsonSchema(schemas.schema_allUsers); 
        
        userId = response.body[1].id;
    });

    it(`should return 200 status code and log into system`, async () => {
           
        let response = await auth.login(email, password);
        accessToken = response.body.token.accessToken.token;
     
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
    });

    it(`should return 200 status code and current user`, async () => {
        let response = await users.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        checkId(response, userId);
        expect(response.body).to.be.jsonSchema(schemas.schema_allUsers);       
    });

    it(`should return 204 status code and update users data`, async () => {
        let response = await users.updateUser(userData, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response,1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_allUsers); 
        
        userId = response.body[1].id;
    });

    it(`should return 200 status code and current user`, async () => {
        let response = await users.getCurrentUser(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        checkId(response, userId);
        expect(response.body).to.be.jsonSchema(schemas.schema_allUsers);       
    });

    it(`should return 200 status code and user with specified id`, async () => {
        let response = await users.getUserById(userId);

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_allUsers);       
    });

    it(`should return 204 status code and delete user with specified id`, async () => {
        let response = await users.deleteUser(userId, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response,1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_allUsers);       
    });


});
