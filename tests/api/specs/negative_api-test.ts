import { expect } from "chai";
import { PostController } from "../lib/controllers/posts.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { xdescribe, before, it, after } from "node:test";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const post = new PostController();
const auth = new AuthController();
const register = new RegisterController();
const users = new UsersController();
let userId: number;


xdescribe("Token usage", () => {
    let invalidEmailDataSet = [
        {email: 'kyryllo16gmail.com', password: 'p@ssword'},
        {email: 'gmail.com@kyryllo16', password: 'p@ssword'},
        {email: 'kyryllo16 @gmail.com', password: 'p@ssword'},
        {email: 'куrуllо16@gmail.com', password: 'p@ssword'}, //cyrillic characters
        {email: 'kyryllo16@gmail.com', password: '"p@ssword"'},
    ]

    let invalidIdDataSet = [
        {authorId: -2, entityId: -2},
        {authorId: 0, entityId: 0},
        {authorId: 0.4, entityId: 0.4},
        {authorId: 999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999, entityId: 1},
        {authorId: "IV", entityId: "XIII"},

    ]


    let accessToken: string;

    before( async () => {
        let response = await auth.login("kyryllo16@gmail.com", "p@ssword");
 
        userId = response.body[1].id;
        accessToken = response.body.token.accessToken.token;
    });

    invalidEmailDataSet.forEach((credentials) => {
        it(`should not login using incorrect credentials : '${credentials.email}' + '${credentials.password}'` , async () => {
            let response = await auth.login(credentials.email, credentials.password);
    
            checkStatusCode(response, 401);
            checkResponseTime(response, 3000);
        });
    })
    
    invalidIdDataSet.forEach((credentials) => {
        it(`should not like post using incorrect credentials : '${credentials.authorId}' + '${credentials.entityId}'`, async () => {
            let response = await post.likePost(credentials.authorId, credentials.entityId);
    
            checkStatusCode(response, 401);
            checkResponseTime(response, 3000);
        });
    })

    

    it(`should not register user using invalid credentials email: null, password: null`, async () => {
        let response = await register.registerUser(null, null);

        checkStatusCode(response, 401);
        checkResponseTime(response, 3000);
    });

   after (async ()=> {
    let response = await users.deleteUser(userId, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response,1000);
   });
});
